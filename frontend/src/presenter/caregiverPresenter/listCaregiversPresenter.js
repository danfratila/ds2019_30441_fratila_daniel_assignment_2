import caregiverModel from "../../model/caregiverModel";

class ListCaregiversPresenter {
    onInit() {
        caregiverModel.loadCaregivers();
    }
}

const listCaregiversPresenter = new ListCaregiversPresenter();

export default listCaregiversPresenter;
