import medicationModel from "../../model/medicationModel";

class DeleteMedicationPresenter {
    onDelete(){
        medicationModel.deleteMedication(medicationModel.state.deleteName)
            .then(() => medicationModel.loadMedications())
            .then(() => medicationModel.changeDeleteMedicationProperty(""));
    }

    onChangeDeleteName(value){
        medicationModel.changeDeleteMedicationProperty(value);
    }
}

const deleteMedicationsPresenter = new DeleteMedicationPresenter();

export default deleteMedicationsPresenter;
