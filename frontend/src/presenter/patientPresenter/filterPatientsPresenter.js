import patientModel from "../../model/patientModel";

class FilterPatientsPresenter {
    onFilterByName(){
        patientModel.clearPreviousFilteredPatients();
        patientModel.filterByName();
    }

    onChangeFilter(value){
        patientModel.changeFilterProperty(value);
    }

    onClearFilters(){
        patientModel.clearFilters();
    }
}

const filterPatientsPresenter = new FilterPatientsPresenter();

export default filterPatientsPresenter;
