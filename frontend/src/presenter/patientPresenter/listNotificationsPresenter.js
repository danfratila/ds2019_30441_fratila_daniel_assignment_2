import patientModel from "../../model/patientModel";


class ListNotificationsPresenter {
    onInit() {
        patientModel.loadNotifications();
    }
}

const listNotificationsPresenter = new ListNotificationsPresenter();

export default listNotificationsPresenter;
