
class WelcomeDoctorPresenter {

    onGoToPatients(){
        window.location.assign("#/doctor/patients");
    }

    onGoToCaregivers(){
        window.location.assign("#/doctor/caregivers");
    }

    onGoToMedications(){
        window.location.assign("#/doctor/medications");
    }
}

const welcomeDoctorPresenter = new WelcomeDoctorPresenter();

export default welcomeDoctorPresenter;
