import React, { Component } from "react";
import caregiverModel from "../../model/caregiverModel";

import CaregiverCreate from "../caregiverView/CaregiverCreate";
import CaregiverDelete from "../caregiverView/CaregiverDelete";
import CaregiverFilter from "../caregiverView/CaregiverFilter";
import CaregiverUpdate from "../caregiverView/CaregiverUpdate";
import CaregiversList from "../caregiverView/CaregiversList";

import createCaregiverPresenter from "../../presenter/caregiverPresenter/createCaregiverPresenter"
import deleteCaregiverPresenter from "../../presenter/caregiverPresenter/deleteCaregiverPresenter"
import filterCaregiversPresenter from "../../presenter/caregiverPresenter/filterCaregiversPresenter"
import listCaregiversPresenter from "../../presenter/caregiverPresenter/listCaregiversPresenter"
import updateCaregiverPresenter from "../../presenter/caregiverPresenter/updateCaregiverPresenter"
import logUserPresenter from "../../presenter/userPresenter/logUserPresenter";


const mapModelStateToComponentState = modelState => ({
    caregivers: modelState.caregivers,
    filteredCaregivers: modelState.filteredCaregivers,

    newFirstname: modelState.newCaregiver.firstname,
    newLastname: modelState.newCaregiver.lastname,
    newBirthdate: modelState.newCaregiver.birthdate,
    newGender: modelState.newCaregiver.gender,
    newAddress: modelState.newCaregiver.address,

    updatedId: modelState.updatedCaregiver.caregiverId,
    updatedFirstname: modelState.updatedCaregiver.firstname,
    updatedLastname: modelState.updatedCaregiver.lastname,
    updatedBirthdate: modelState.updatedCaregiver.birthdate,
    updatedGender: modelState.updatedCaregiver.gender,
    updatedAddress: modelState.updatedCaregiver.address,

    filterTag: modelState.filterTag,
    deleteId: modelState.deleteId
});

export default class SmartCaregivers extends Component {
    constructor() {
        super();
        this.state = mapModelStateToComponentState(caregiverModel.state);
        this.listener = modelState => this.setState(mapModelStateToComponentState(modelState));
        caregiverModel.addListener("change", this.listener);
        listCaregiversPresenter.onInit();
    }

    componentWillUnmount() {
        caregiverModel.removeListener("change", this.listener);
    }

    render() {
        return (
            <div>
                {/*Validate user every time a page is loaded*/}
                {logUserPresenter.onValidateLoggedUser()}

                {/*Create*/}
                <CaregiverCreate
                    firstname={this.state.newFirstname}
                    lastname={this.state.newLastname}
                    birthDate={this.state.newBirthdate}
                    gender={this.state.newGender}
                    caregiverAddress={this.state.newAddress}
                    onChange={createCaregiverPresenter.onChange}
                    onCreate={createCaregiverPresenter.onCreate}
                />

                {/*Read all*/}
                <CaregiversList
                    caregivers={this.state.caregivers}
                />

                {/*Filter (Search by first name)*/}
                <CaregiverFilter
                    filteredName={this.state.filterTag}
                    onChangeFilteredName={filterCaregiversPresenter.onChangeFilter}
                    onFilter={filterCaregiversPresenter.onFilterByName}
                    onClearFilters={filterCaregiversPresenter.onClearFilters}
                />

                {/*Read filtered*/}
                <CaregiversList
                    title={"Filtered caregivers"}
                    caregivers={this.state.filteredCaregivers}
                />

                {/*Update*/}
                <CaregiverUpdate
                    id={this.state.updatedId}
                    firstname={this.state.updatedFirstname}
                    lastname={this.state.updatedLastname}
                    birthDate={this.state.updatedBirthdate}
                    gender={this.state.updatedGender}
                    caregiverAddress={this.state.updatedAddress}
                    onUpdateCaregiver={updateCaregiverPresenter.onUpdateCaregiver}
                    onChange={updateCaregiverPresenter.onChange}
                />

                {/*Delete*/}
                <CaregiverDelete
                    deleteId={this.state.deleteId}
                    onChangeDeleteId={deleteCaregiverPresenter.onChangeDeleteId}
                    onDelete={deleteCaregiverPresenter.onDelete}
                />
            </div>
        );
    }
}
