import React, { Component } from "react";
import medicationModel from "../../model/medicationModel";

import createMedicationPresenter from "../../presenter/medicationPresenter/createMedicationPresenter";
import deleteMedicationPresenter from "../../presenter/medicationPresenter/deleteMedicationPresenter";
import filterMedicationsPresenter from "../../presenter/medicationPresenter/filterMedicationsPresenter";
import listMedicationsPresenter from "../../presenter/medicationPresenter/listMedicationsPresenter";
import updateMedicationPresenter from "../../presenter/medicationPresenter/updateMedicationPresenter";

import MedicationCreate from "./MedicationCreate";
import MedicationDelete from "./MedicationDelete";
import MedicationFilter from "./MedicationFilter";
import MedicationsList from "./MedicationsList";
import MedicationUpdate from "./MedicationUpdate";
import logUserPresenter from "../../presenter/userPresenter/logUserPresenter";



const mapModelStateToComponentState = modelState => ({
    newName: modelState.newMedication.name,
    newSideEffects: modelState.newMedication.sideEffects,
    newDosage: modelState.newMedication.dosage,
    updatedName: modelState.updatedMedication.name,
    updatedSideEffects: modelState.updatedMedication.sideEffects,
    updatedDosage: modelState.updatedMedication.dosage,

    medications: modelState.medications,
    filterTag: modelState.filterTag,
    filteredMedications: modelState.filteredMedications,
    deleteName: modelState.deleteName
});

export default class SmartMedications extends Component {
    constructor() {
        super();
        this.state = mapModelStateToComponentState(medicationModel.state);
        this.listener = modelState => this.setState(mapModelStateToComponentState(modelState));
        medicationModel.addListener("change", this.listener);
        listMedicationsPresenter.onInit();
    }

    componentWillUnmount() {
        medicationModel.removeListener("change", this.listener);
    }

    render() {
        return (
            <div>
                {/*Validate user every time a page is loaded*/}
                {logUserPresenter.onValidateLoggedUser()}

                {/*Create*/}
                <MedicationCreate
                    newName={this.state.newName}
                    newSideEffects={this.state.newSideEffects}
                    newDosage={this.state.newDosage}
                    onChange={createMedicationPresenter.onChange}
                    onCreate={createMedicationPresenter.onCreate}
                />

                {/*Read all*/}
                <MedicationsList
                    medications={this.state.medications}
                />

                {/*Filter (Search by name)*/}
                <MedicationFilter
                    filteredName={this.state.filterTag}
                    onChangeFilteredName={filterMedicationsPresenter.onChangeFilter}
                    onFilter={filterMedicationsPresenter.onFilterByName}
                    onClearFilters={filterMedicationsPresenter.onClearFilters}
                />

                {/*Read filtered*/}
                <MedicationsList
                    title={"Filtered medications"}
                    medications={this.state.filteredMedications}
                />

                {/*Update*/}
                <MedicationUpdate
                    updatedName={this.state.updatedName}
                    updatedSideEffects={this.state.updatedSideEffects}
                    updatedDosage={this.state.updatedDosage}
                    onUpdateMed={updateMedicationPresenter.onUpdateMed}
                    onChange={updateMedicationPresenter.onChange}
                />

                {/*Delete*/}
                <MedicationDelete
                    deleteName={this.state.deleteName}
                    onChangeDeleteName={deleteMedicationPresenter.onChangeDeleteName}
                    onDelete={deleteMedicationPresenter.onDelete}
                />
            </div>
        );
    }
}
