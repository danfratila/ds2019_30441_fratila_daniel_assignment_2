import React from "react";

const PatientsList = ({ patients, title, refresh}) => (
    <div>
        <h2>{ title || "Patients" }</h2>
        <table border="1">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>First name</th>
                    <th>Last name</th>
                    <th>Birth date</th>
                    <th>Gender</th>
                    <th>Address</th>
                    <th>Record</th>
                </tr>
            </thead>
            <tbody>
                {
                    patients.map((patient, index) => (
                        <tr key={index} >
                            <td>{patient.id}</td>
                            <td>{patient.firstname}</td>
                            <td>{patient.surname}</td>
                            <td>{patient.birthdate}</td>
                            <td>{patient.gender}</td>
                            <td>{patient.address}</td>
                            <td>{patient.record}</td>
                        </tr>
                    ))
                }
            </tbody>
        </table>

        <br/>
        <button onClick={refresh}>Refresh list</button>
    </div>
);

export default PatientsList;
