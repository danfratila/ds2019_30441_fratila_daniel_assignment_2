import React, { Component } from "react";

import patientModel from "../../../model/patientModel";
import logUserPresenter from "../../../presenter/userPresenter/logUserPresenter";
import PatientWelcome from "./PatientWelcome";
import PatientNotificationsList from "./PatientNotificationsList";
import listNotificationsPresenter from "../../../presenter/patientPresenter/listNotificationsPresenter";

const mapModelStateToComponentState = modelState => ({
    notifications: modelState.notifications
});

export default class SmartPatientWelcome extends Component {
    constructor() {
        super();
        this.state = mapModelStateToComponentState(patientModel.state);
        this.listener = modelState => this.setState(mapModelStateToComponentState(modelState));
        patientModel.addListener("change", this.listener);
        listNotificationsPresenter.onInit();
    }

    componentWillUnmount() {
        patientModel.removeListener("change", this.listener);
    }

    render() {
        return (
            <div>
                {/*Validate user every time a page is loaded*/}
                {logUserPresenter.onValidateLoggedUser()}

                <PatientWelcome
                />

                <PatientNotificationsList
                    notifications={this.state.notifications}
                />
            </div>
        );
    }
}
