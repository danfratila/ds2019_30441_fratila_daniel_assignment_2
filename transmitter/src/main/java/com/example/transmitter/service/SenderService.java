package com.example.transmitter.service;

import com.example.transmitter.entity.PatientData;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;


@Component
public class SenderService implements CommandLineRunner {

    private static final String QUEUE_NAME = "q";
    private static final String PATIENT_ID = "1";
    private static final String FILE_PATH = "C:\\Users\\Daneeee\\Desktop\\transmitter\\src\\main\\resources\\activity.txt";
    private static final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";

    @Override
    public void run(String... args) throws Exception {
        List<PatientData> patientDataList = new ArrayList<>();

        try (Stream<String> stream = Files.lines((Paths.get(FILE_PATH))))
        {
            stream.forEach(
                    line->{
                        String[] parts = line.split("\t\t");
                        PatientData pd = new PatientData();
                        try
                        {
                            pd.setPatient_id(PATIENT_ID);
                            pd.setStartTime(new SimpleDateFormat(DATE_FORMAT).parse(parts[0]).getTime());
                            pd.setEndTime(new SimpleDateFormat(DATE_FORMAT).parse(parts[1]).getTime());
                            pd.setActivity(parts[2]);

                            patientDataList.add(pd);
                        }
                        catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
            );
        }
        catch (IOException e) {
            e.printStackTrace();
        }


        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        try (Connection connection = factory.newConnection();
             Channel channel = connection.createChannel())
        {
            channel.queueDeclare(QUEUE_NAME, false, false, false, null);
            String message = patientDataList.toString();
            channel.basicPublish("", QUEUE_NAME, null, message.getBytes("UTF-8"));
            System.out.println(" [x] Sent '" + message + "'");
        }
    }
}
