package com.example.whatever.controllers;

import com.example.whatever.dtos.PatientDataDTO;
import com.example.whatever.services.ReceiverService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeoutException;


@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping("/patientdata")
public class PatientDataController {

    @Autowired
    private ReceiverService receiverService;


    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public List<PatientDataDTO> getAllPatientInfos() throws IOException, TimeoutException {
        return receiverService.findAll();
    }

}


