package com.example.whatever.repos;

import com.example.whatever.entities.PatientData;
import org.springframework.data.jpa.repository.JpaRepository;


public interface PatientDataRepo extends JpaRepository<PatientData, Integer> {


}