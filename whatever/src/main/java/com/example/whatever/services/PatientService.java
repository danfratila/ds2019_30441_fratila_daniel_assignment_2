package com.example.whatever.services;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import com.example.whatever.dtos.PatientDTO;
import com.example.whatever.entities.Patient;
import com.example.whatever.entities.User;
import com.example.whatever.event.BaseEvent;
import com.example.whatever.repos.CaregiverRepo;
import com.example.whatever.repos.PatientRepo;
import com.example.whatever.repos.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.event.EventListener;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;


import static com.example.whatever.services.UtilityServices.extractNames;
import static com.example.whatever.services.UtilityServices.getNormalDateFromString;


@Service
public class PatientService {

	@Autowired
	private UserRepo usrRepository;
	
	@Autowired
	private PatientRepo patientRepository;
	
	@Autowired
	private CaregiverRepo caregiverRepository;


	public PatientDTO findPatientById(int id) {
		Optional<User> usr = usrRepository.findById(id);
		Optional<Patient> patient = patientRepository.findById(id);
		PatientDTO dto = new PatientDTO();
		if (!usr.isPresent() || !patient.isPresent()) {
			//throw new ResourceNotFoundException(User.class.getSimpleName());
		}
		else{
			String[] names = extractNames(usr.get().getName());
			dto = new PatientDTO.Builder()
					.id(usr.get().getId())
					.firstname(names[0])
					.surname(names[1])
					.birthdate(usr.get().getBirthdate())
					.gender(usr.get().getGender())
					.address(usr.get().getAddress())
					.record(patient.get().getMedicalRecord())
					.create();
		}
		return dto;
	}


	public List<PatientDTO> findAll() {
		List<Patient> patients = patientRepository.findAll();
		List<PatientDTO> toReturn = new ArrayList<>();
		for (Patient patient: patients) {
			Optional<User> user = usrRepository.findById(patient.getId());
			if(user.isPresent()){
				String[] names = extractNames(user.get().getName());
				toReturn.add(new PatientDTO.Builder()
						.id(user.get().getId())
						.firstname(names[0])
						.surname(names[1])
						.birthdate(user.get().getBirthdate())
						.gender(user.get().getGender())
						.address(user.get().getAddress())
						.record(patient.getMedicalRecord())
						.create());
			}
		}
		return toReturn;
	}


	public int create(PatientDTO patientDTO) {
		User user = new User();
		
		user.setName(patientDTO.getFirstname() + " " + patientDTO.getSurname());
		Date date = UtilityServices.getNormalDateFromString(patientDTO.getBirthdate());
		user.setBirthdate(date);
		user.setGender(patientDTO.getGender());
		user.setAddress(patientDTO.getAddress());
		
		User newUser = usrRepository.save(user);
		
		Patient patient = new Patient();
		patient.setId(newUser.getId());
		patient.setMedicalRecord(patientDTO.getRecord());
		
		Patient ptnt = patientRepository.save(patient);
		return ptnt.getId();
	}


	public List<PatientDTO> findPatientByName(String name) {
		// TODO Auto-generated method stub
		List<PatientDTO> dtos = new ArrayList<PatientDTO>();
		List<User> users = usrRepository.findAll(); 
		for(User user: users) {
			if(patientRepository.findById(user.getId()).isPresent()
					&& user.getName().toLowerCase().contains(name.toLowerCase())) {
				String[] names = extractNames(user.getName());
				PatientDTO dto = new PatientDTO.Builder()
						.id(user.getId())
						.firstname(names[0])
						.surname(names[1])
						.birthdate(user.getBirthdate())
						.gender(user.getGender())
						.address(user.getAddress())
						.record(patientRepository.findById(user.getId()).get().getMedicalRecord())
						.create();
				dtos.add(dto);
			}
		}
		return dtos;
	}


	public void update(Integer id, PatientDTO patientDTO) {
		// TODO Auto-generated method stub
		Optional<User> user = usrRepository.findById(id); // normally same with patient dto
		if(user.isPresent()) {
			String[] names = extractNames(user.get().getName());
			Optional<Patient> patient = patientRepository.findById(user.get().getId());
			if(patient.isPresent()){
				if(patientDTO.getFirstname() != null && !patientDTO.getFirstname().equals("")) {
					names[0] = patientDTO.getFirstname();
				}
				if(patientDTO.getSurname() != null && !patientDTO.getSurname().equals("")) {
					names[1] = patientDTO.getSurname();
				}
				user.get().setName(names[0] + " " + names[1]);
				if(patientDTO.getBirthdate() != null && !patientDTO.getBirthdate().equals("")) {
					user.get().setBirthdate(getNormalDateFromString(patientDTO.getBirthdate()));
				}
				if(patientDTO.getGender() != null && !patientDTO.getGender().equals("")) {
					user.get().setGender(patientDTO.getGender());
				}
				if(patientDTO.getAddress() != null && !patientDTO.getAddress().equals("")) {
					user.get().setAddress(patientDTO.getAddress());
				}
				usrRepository.save(user.get());


				if(patientDTO.getRecord() != null && !patientDTO.getRecord().equals("")) {
					patient.get().setMedicalRecord(patientDTO.getRecord());
					patientRepository.save(patient.get());
				}
			}
		}
	}

	public PatientDTO delete(Integer id) {
		Optional<User> user = usrRepository.findById(id);
		if(user.isPresent()) {
			usrRepository.delete(user.get());
			patientRepository.delete(patientRepository.findById(id).get());
		}
		return new PatientDTO();
	}
	
	private List<Integer> extractIds(String listOfIds){
		List<Integer> ids = new ArrayList<Integer>();
		String[] myList = listOfIds.split(",");
		
		for(String s: myList) {
			ids.add(Integer.parseInt(s.trim()));
		}
		
		return ids;
	}

	public List<PatientDTO> findSpecificPatients(String username) {
		// TODO Auto-generated method stub
		Optional<User> requesterUser = usrRepository.findByUsername(username);
		String listOfIds = caregiverRepository.findById(requesterUser.get().getId()).get().getPatients();
		List<PatientDTO> dtos = new ArrayList<>();
		List<Integer> ids = extractIds(listOfIds);
		for(Integer i: ids) {
			Optional<User> u = usrRepository.findById(i);
			Optional<Patient> p = patientRepository.findById(i);
			if(u.isPresent() && p.isPresent()) {
				String[] names = extractNames(u.get().getName());
				PatientDTO dto = new PatientDTO.Builder()
						.id(u.get().getId())
						.firstname(names[0])
						.surname(names[1])
						.birthdate(u.get().getBirthdate())
						.gender(u.get().getGender())
						.address(u.get().getAddress())
						.record(p.get().getMedicalRecord())
						.create();
				dtos.add(dto);
			}
		}
		return dtos;
	}
}
