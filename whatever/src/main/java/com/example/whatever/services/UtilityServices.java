package com.example.whatever.services;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


class UtilityServices {

    private static final String SPLIT_CH = " ";

    static String[] extractNames(String fullname){
        String[] names = new String[2];
        int surnameIndex = fullname.lastIndexOf(SPLIT_CH);
        names[0] = fullname;
        names[1] = "";
        if (surnameIndex != -1) {
            names[0] = fullname.substring(0, surnameIndex).trim();
            names[1] = fullname.substring(surnameIndex).trim();
        }
        return names;
    }

    static Date getNormalDateFromString(String givenDateString){
        Date date = new Date();
        try {
            System.out.println(date);
            date = new SimpleDateFormat("yyyy-MM-dd").parse(givenDateString);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return date;
    }

}
